<?php

    session_start();
    if(!$_SESSION["ingreso"]){
        header("location:index.php?ruta=ingreso");
        exit();
    }

?>

<div class="row">
    <div class="col s12 ">
        <h1 class="center">Empleados</h1>
    </div>
</div>

<div class="row">
    <table class="highlight centered responsive-table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Puesto</th>
                <th>Salario</th>
                <th></th>
                <th></th>
            </tr>
        </thead>

        <tbody>
            
            <?php
            
                $mostrar = new EmpleadosC();
                $mostrar -> MostrarEmpleadosC();
            
            ?>
            
        </tbody>

    </table>    
</div>

    <?php
    
        $eliminar = new EmpleadosC();
        $eliminar -> BorrarEmpleadoC();
    
    ?>