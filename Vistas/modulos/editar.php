<?php

    session_start();
    if(!$_SESSION["ingreso"]){
        header("location:index.php?ruta=ingreso");
        exit();
    }

?>

<div class="row">
    <div class="col s12">
        <h1 class="center">Editar Empleado</h1>
    </div>
</div>

<div class="row center">
    <form method="post" class="col s10 m8 center offset-m2 offset-s1">    
        
        <?php

            $editar = new EmpleadosC();
            $editar -> EditarEmpleadoC();

            $actualizar = new EmpleadosC();
            $actualizar -> ActualizarEmpleadoC();
        ?>

    </form>
</div>
