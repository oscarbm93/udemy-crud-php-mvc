<div class="row">
    <div class="col s12">
        <h1 class="center">Ingresar</h1>
    </div>
</div>

<div class="row">
    <form class="col s12" method="post" action="">
        <div class="row">
            <div class="input-field col s10 m8 offset-m2 offset-s1">
                <label for="usuarioI">Usuario </label>
                <input type="text" name="usuarioI" class="validate">
                <span class="helper-text" data-error="wrong" data-success="right">Tu usuario debe contener 8 caracteres</span>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s10 m8 offset-m2 offset-s1">
                <label for="claveI">Contraseña </label>
                <input type="password" name="claveI" class="validate">
                <span class="helper-text" data-error="wrong" data-success="right">Tu contraseña debe contener al menos 6 caracteres</span>
            </div>
        </div>
        <div class="row">
            <div class="col s12 center">
                <button class="center btn-large waves-effect waves-light green lighten-2" type="submit" name="action">Entrar
                    <i class="material-icons right">check</i>
                </button>
            </div>
        </div>
    </div>
</div>

<?php
    
    $ingreso = new AdminC();
    $ingreso -> IngresoC();

?>