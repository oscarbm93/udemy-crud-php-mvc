<?php

    session_start();
    if(!$_SESSION["ingreso"]){
        header("location:index.php?ruta=ingreso");
        exit();
    }

?>

<div class="row">
    <div class="col s12">
        <h1 class="center">Registrar Empleado</h1>
    </div>
</div>

<div class="row center">
    <form method="post" class="col s10 m8 center offset-m2 offset-s1">    
        <div class="row">
            <div class="input-field">
                <input type="text" name="nombreR" class="validate">
                <label for="nombreR">Nombre</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field">
                <input type="text" name="apellidoR" id="" class="validate">
                <label for="apellidoR">Apellido</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field">
                <input type="email" name="emailR" id="" class="validate">
                <label for="emailR">Email</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field">
                <input type="text" name="puestoR" id="" class="validate">
                <label for="puestoR">Puesto</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field">
                <input type="text" name="salarioR" id="" class="validate">
                <label for="salarioR">Salario</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field">
                <button class="center btn-large waves-effect waves-light green lighten-2" type="submit" name="action">Registrar
                    <i class="material-icons right">check</i>
                </button>
            </div>
        </div>    
    </form>
</div>

<?php

    $registrar = new EmpleadosC();
    $registrar -> RegistrarEmpleadosC()

?>