<?php

    require_once "conexionBD.php";

    class EmpleadosM extends ConexionBD{

        // Registrar empleados
        static public function RegistrarEmpleadosM($datosC, $tablaBD){

            $pdo = ConexionBD::cBD()->prepare("INSERT INTO $tablaBD (NOMBRE, APELLIDO, EMAIL, PUESTO, SALARIO) VALUES (:nombre, :apellido, :email, :puesto, :salario)");
            
            $pdo -> bindParam(":nombre", $datosC["nombre"], PDO::PARAM_STR);
            $pdo -> bindParam(":apellido", $datosC["apellido"], PDO::PARAM_STR);
            $pdo -> bindParam(":email", $datosC["email"], PDO::PARAM_STR);
            $pdo -> bindParam(":puesto", $datosC["puesto"], PDO::PARAM_STR);
            $pdo -> bindParam(":salario", $datosC["salario"], PDO::PARAM_STR);

            if ($pdo -> execute()){
                return "ok";
            } else{
                return "ko";
            }

            $pdo -> close();
        }

        // Mostrar empleados
        static public function MostrarEmpleadosM($tablaBD){

            $pdo = ConexionBD::cBD()->prepare("SELECT ID, NOMBRE, APELLIDO, EMAIL, PUESTO, SALARIO FROM $tablaBD");

            $pdo -> execute();

            return $pdo -> fetchAll();

            $pdo -> close();

        }

        // Editar empleado
        static public function EditarEmpleadoM($datosC, $tablaBD){

            $pdo = ConexionBD::cBD()->prepare("SELECT ID, NOMBRE, APELLIDO, EMAIL, PUESTO, SALARIO FROM $tablaBD WHERE ID = :id");

            $pdo -> bindParam(":id", $datosC, PDO::PARAM_INT);

            $pdo -> execute();

            return $pdo -> fetch();

            $pdo -> close();

        }

        // Actualizar empleado
        static public function ActualizarEmpleadoM($datosC, $tablaBD){

            $pdo = ConexionBD::cBD()->prepare("UPDATE $tablaBD SET NOMBRE = :nombre, APELLIDO = :apellido, EMAIL = :email, PUESTO = :puesto, SALARIO = :salario WHERE ID = :id");

            $pdo -> bindParam(":id", $datosC["id"], PDO::PARAM_INT);
            $pdo -> bindParam(":nombre", $datosC["nombre"], PDO::PARAM_STR);
            $pdo -> bindParam(":apellido", $datosC["apellido"], PDO::PARAM_STR);
            $pdo -> bindParam(":email", $datosC["email"], PDO::PARAM_STR);
            $pdo -> bindParam(":puesto", $datosC["puesto"], PDO::PARAM_STR);
            $pdo -> bindParam(":salario", $datosC["salario"], PDO::PARAM_STR);

            if ($pdo -> execute()){
                return "ok";
            } else{
                return "ko";
            }

            $pdo -> close();
        }

        // Borrar empleado
        static public function BorrarEmpleadoM($datosC, $tablaBD){

            $pdo = ConexionBD::cBD()->prepare("DELETE FROM EMPLEADOS WHERE ID = :id");

            $pdo -> bindParam(":id", $datosC, PDO::PARAM_INT);

            if ($pdo -> execute()){
                return "ok";
            } else{
                return "ko";
            }

            $pdo -> close();            

        }
    }

?>