<?php

    require_once "conexionBD.php";

    class AdminM extends ConexionBD{

        public static function IngresoM($datosC, $tablaBD){

            // Preparamos la consulta
            $pdo = ConexionBd::cBD()->prepare("SELECT USUARIO, CLAVE FROM $tablaBD WHERE USUARIO = :usuario");

            // Bindeamos el parámetro del array
            $pdo -> bindParam(":usuario", $datosC["usuario"], PDO::PARAM_STR);

            // Ejecutamos
            $pdo -> execute();
            
            // Devolvemos la consulta, fetch() porque solo es uno, sino fetch_all()
            return $pdo->fetch();

            // Cerramos la conexión
            $pdo -> close();
        }

    }

?>