<?php

    class EmpleadosC{

        // Registramos empleados
        public function RegistrarEmpleadosC(){
            if (isset($_POST["nombreR"])){
                $datosC = array("nombre"=>$_POST["nombreR"], "apellido"=>$_POST["apellidoR"], "email"=>$_POST["emailR"], 
                                            "puesto"=>$_POST["puestoR"], "salario"=>$_POST["salarioR"]);
                $tablaBD = "empleados";

                $respuesta = EmpleadosM::RegistrarEmpleadosM($datosC, $tablaBD);

                if ($respuesta == "ok"){
                    header("location:index.php?ruta=empleados");
                }
                else{
                    echo "error";
                }
            }
        }

        // Mostrar empleados
        public function MostrarEmpleadosC(){

            $tablaBD = "empleados";

            $respuesta = EmpleadosM::MostrarEmpleadosM($tablaBD);

            foreach ($respuesta as $key => $value) {
                
                echo '
                <tr>
                    <td>'.$value["NOMBRE"].'</td>
                    <td>'.$value["APELLIDO"].'</td>
                    <td>'.$value["EMAIL"].'</td>
                    <td>'.$value["PUESTO"].'</td>
                    <td>'.$value["SALARIO"].'</td>
                    <td><a href="index.php?ruta=editar&id='.$value["ID"].'"><button class="btn-flat"><i class="material-icons">create</i></button></a></td>
                    <td><a href="index.php?ruta=empleados&idB='.$value["ID"].'"><button class="btn-flat"><i class="material-icons">delete</i></button></a></td>
                </tr>
                ';
            }

        }

        // Editar empleado
        public function EditarEmpleadoC(){
            $datosC = $_GET["id"];

            $tablaBD = "empleados";

            $respuesta = EmpleadosM::EditarEmpleadoM($datosC, $tablaBD);

            echo '
            <input type="hidden" name="idE" value="'.$respuesta["ID"].'">
            <div class="row">
                <div class="input-field">
                    <input type="text" name="nombreE" class="validate" value="'.$respuesta["NOMBRE"].'">
                    <label for="nombreR">Nombre</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field">
                    <input type="text" name="apellidoE" id="" class="validate" value="'.$respuesta["APELLIDO"].'">
                    <label for="apellidoR">Apellido</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field">
                    <input type="email" name="emailE" id="" class="validate" value="'.$respuesta["EMAIL"].'">
                    <label for="emailR">Email</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field">
                    <input type="text" name="puestoE" id="" class="validate" value="'.$respuesta["PUESTO"].'">
                    <label for="puestoR">Puesto</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field">
                    <input type="text" name="salarioE" id="" class="validate" value="'.$respuesta["SALARIO"].'">
                    <label for="salarioR">Salario</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field">
                    <button class="center btn-large waves-effect waves-light green lighten-2" type="submit" name="action">Actualizar
                        <i class="material-icons right">create</i>
                    </button>
                </div>
            </div>    
            ';
        }

        // Actualizar empleado
        public function ActualizarEmpleadoC(){

            if (isset($_POST["nombreE"])){

                $datosC = array("id"=>$_POST["idE"], "nombre"=>$_POST["nombreE"], "apellido"=>$_POST["apellidoE"], 
                    "email"=>$_POST["emailE"], "puesto"=>$_POST["puestoE"], "salario"=>$_POST["salarioE"]);

                var_dump($datosC);

                $tablaBD = "empleados";

                $respuesta = EmpleadosM::ActualizarEmpleadoM($datosC, $tablaBD);

                if ($respuesta == "ok"){
                    header("location:index.php?ruta=empleados");
                }
                else{
                    echo "error";
                }
            }

        }

        // Eliminar empleado
        function BorrarEmpleadoC(){

            if (isset($_GET["idB"])){
            
                $datosC = $_GET["idB"];

                $tablaBD = "empleados";

                $respuesta = EmpleadosM::BorrarEmpleadoM($datosC, $tablaBD);

                if ($respuesta == "ok"){
                    header("location:index.php?ruta=empleados");
                }
                else{
                    echo "error";
                }
            }

        }

    }

?>